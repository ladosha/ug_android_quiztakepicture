package net.bytesly.ug_androidquiz_takepicture;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.core.content.res.ResourcesCompat;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    ImageView imageViewOne;
    ImageView imageViewTwo;
    ImageView imageViewThree;
    ImageView imageViewFour;

    Button buttonNext;

    Set<Integer> processedImageViews;

    ActivityResultLauncher<Uri> activityResultLauncherImageViewOne;
    ActivityResultLauncher<Uri> activityResultLauncherImageViewTwo;
    ActivityResultLauncher<Uri> activityResultLauncherImageViewThree;
    ActivityResultLauncher<Uri> activityResultLauncherImageViewFour;

    Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageViewOne = findViewById(R.id.imageViewOne);
        imageViewTwo = findViewById(R.id.imageViewTwo);
        imageViewThree = findViewById(R.id.imageViewThree);
        imageViewFour = findViewById(R.id.imageViewFour);

        buttonNext = findViewById(R.id.buttonNext);

        processedImageViews = new HashSet<>();

        imageViewOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePictureFromCamera(activityResultLauncherImageViewOne);
            }
        });
        imageViewTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePictureFromCamera(activityResultLauncherImageViewTwo);
            }
        });
        imageViewThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePictureFromCamera(activityResultLauncherImageViewThree);
            }
        });
        imageViewFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePictureFromCamera(activityResultLauncherImageViewFour);
            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Drawable defaultDrawable = getResources().getDrawable(R.drawable.ic_launcher_foreground);
                if(processedImageViews.size() != 4) {
                    Toast.makeText(MainActivity.this, "Not all images have been set!", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(MainActivity.this, "Success!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        activityResultLauncherImageViewOne = registerForActivityResult(new ActivityResultContracts.TakePicture(), new ActivityResultCallback<Boolean>() {
            @Override
            public void onActivityResult(Boolean result) {
                if (result) {
                    imageViewOne.setImageURI(uri);
                    processedImageViews.add(imageViewOne.getId());
                } else {
                    Toast.makeText(MainActivity.this, "Error!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        activityResultLauncherImageViewTwo = registerForActivityResult(new ActivityResultContracts.TakePicture(), new ActivityResultCallback<Boolean>() {
            @Override
            public void onActivityResult(Boolean result) {
                if (result) {
                    imageViewTwo.setImageURI(uri);
                    processedImageViews.add(imageViewTwo.getId());
                } else {
                    Toast.makeText(MainActivity.this, "Error!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        activityResultLauncherImageViewThree = registerForActivityResult(new ActivityResultContracts.TakePicture(), new ActivityResultCallback<Boolean>() {
            @Override
            public void onActivityResult(Boolean result) {
                if (result) {
                    imageViewThree.setImageURI(uri);
                    processedImageViews.add(imageViewThree.getId());
                } else {
                    Toast.makeText(MainActivity.this, "Error!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        activityResultLauncherImageViewFour = registerForActivityResult(new ActivityResultContracts.TakePicture(), new ActivityResultCallback<Boolean>() {
            @Override
            public void onActivityResult(Boolean result) {
                if (result) {
                    imageViewFour.setImageURI(uri);
                    processedImageViews.add(imageViewFour.getId());
                } else {
                    Toast.makeText(MainActivity.this, "Error!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void takePictureFromCamera(ActivityResultLauncher<Uri> targetLauncher) {
        File path = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "My Pictures");

        if(!path.exists()) {
            path.mkdir();
        }

        File imageFile = null;

        try {
            imageFile = File.createTempFile("MyFile", ".jpg", path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        uri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", imageFile);

        targetLauncher.launch(uri);
    }
}